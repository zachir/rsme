pub mod buffer;
pub mod editor;

extern crate pancurses;

use pancurses::{initscr, endwin, Input, noecho};
use std::env::args;

fn main() {
    let mut filename = "!!!!".to_owned();
    for arg in args() {
        if arg != "rsme" {
            filename = arg;
            break;
        }
    }
    if filename == "!!!!".to_owned() {
        filename = "~/text.txt".to_owned();
    }
    
    let window = initscr();
    window.printw("This is the default line.\n");
    window.refresh();
    window.keypad(true);
    noecho();
    loop {
        match window.getch() {
            Some(Input::Character(c)) => { window.addch(c); },
            Some(Input::KeyDC) => break,
            Some(input) => { window.addstr(&format!("{:?}", input)); },
            None => ()
        }
    }
    endwin();
}
