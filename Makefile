BUILDER=cargo
BUILDCMD=build
BUILDFLAG=--release
CHECKCMD=check
RUNCMD=run
CLEANDIR=target

all: src/main.rs
	$(BUILDER) $(BUILDCMD) $(BUILDFLAG)

check: src/main.rs
	$(BUILDER) $(CHECKCMD) $(BUILDFLAG)

run: src/main.rs
	$(BUILDER) $(RUNCMD) $(BUILDFLAG)

clean:
	rm -rf $(CLEANDIR) Cargo.lock
